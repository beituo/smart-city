- [快速公交信号优先_jm-csu的博客-CSDN博客](https://blog.csdn.net/weijimin1/article/details/88043022)

- ## Active signal priority control method for bus rapid transit based on Vehicle Infrastructure Integration – 2017

- 摘要

1. 减少BRT车辆的延时，使平均乘客收益最大（平均乘客的等待时间最小），针对单个路口进行仿真。采用了8种BRT到达模式，每一种模式对应于采用何种信号优先策略。
2. 通过vissim进行仿真
3. 平均乘客的等待时间减少13.43%-25.27%，BRT的速度有7.1~7.55%的提升。

- 本论文的公交优先主要包含主动检测模块、预测模块、决策模块

  ------

   

主检测模块主要检测BRT车辆的位置、速度、进站等待时间等信息

预测模块主要是预测BRT车辆到达交叉路口停止线的时间

决策模块主要是根据BRT的公交优先请求确定采用哪种信号优先策略-绿灯延长、红灯截断、相位插入或拒绝优先请求。

1. 信号优先的流程图

![img](https://img-blog.csdnimg.cn/20190228194040722.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaWppbWluMQ==,size_16,color_FFFFFF,t_70)

注意：通过计算BRT车辆和社会车辆的PI值的加权来最终决定是否采用信号优先策略。个人理解是如果采用公交优先导致整个社会车辆的延误太大，则不采取公交优先策略。

![img](https://img-blog.csdnimg.cn/20190228194040726.png)

不同信号优先策略的PI值计算

红灯截断

![img](https://img-blog.csdnimg.cn/20190228194040727.png)

![img](https://img-blog.csdnimg.cn/20190228194040724.png)

绿灯延长

![img](https://img-blog.csdnimg.cn/20190228194040729.png)

![img](https://img-blog.csdnimg.cn/20190228194040735.png)

相位插入

![img](https://img-blog.csdnimg.cn/20190228194040734.png)

![img](https://img-blog.csdnimg.cn/20190228194040742.png)

社会车辆的PI值计算

![img](https://img-blog.csdnimg.cn/20190228194040748.png)

![img](https://img-blog.csdnimg.cn/20190228194040752.png)

### 预测BRT到达停止线的时间

![img](https://img-blog.csdnimg.cn/20190228194040761.png)

![img](https://img-blog.csdnimg.cn/20190228194040761.png)

BRT到达停止线的最早和最晚时间

![img](https://img-blog.csdnimg.cn/20190228194040767.png)

BRT到达停止线的时刻对应红绿灯周期上时刻

![img](https://img-blog.csdnimg.cn/20190228194040773.png)

![img](https://img-blog.csdnimg.cn/20190228194040777.png)

 

### BRT到达停止线的时刻对应的八种模式

![img](https://img-blog.csdnimg.cn/20190228194040780.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaWppbWluMQ==,size_16,color_FFFFFF,t_70)

八种模式对应的信号优先策略

![img](https://img-blog.csdnimg.cn/20190228194040795.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaWppbWluMQ==,size_16,color_FFFFFF,t_70)

模式1

红灯截断

红灯截断的时间计算

![img](https://img-blog.csdnimg.cn/20190228194040798.png)

![img](https://img-blog.csdnimg.cn/20190228194040811.png)

模式2

红绿灯相位保持不变

模式3

采用绿灯延长

绿灯延长时间为

![img](https://img-blog.csdnimg.cn/20190228194040817.png)

![img](https://img-blog.csdnimg.cn/20190228194040816.png)

模式4和模式5

绿灯延长

绿灯延长时间

![img](https://img-blog.csdnimg.cn/20190228194040809.png)

模式6

相位插入

![img](https://img-blog.csdnimg.cn/20190228194040820.png)

![img](https://img-blog.csdnimg.cn/20190228194040818.png)

模式7

红灯截断

![img](https://img-blog.csdnimg.cn/20190228194040840.png)

模式8

![img](https://img-blog.csdnimg.cn/20190228194040845.png)

### 仿真

采用vissim5.2和C#进行联合仿真

![img](https://img-blog.csdnimg.cn/20190228194040893.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaWppbWluMQ==,size_16,color_FFFFFF,t_70)

结果

![img](https://img-blog.csdnimg.cn/20190228194040865.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaWppbWluMQ==,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20190228194040867.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaWppbWluMQ==,size_16,color_FFFFFF,t_70)

结论：不同车流量的情况下采用TSP方案都能够减少平均每个乘客的等待时间和提高BRT的运行速度。